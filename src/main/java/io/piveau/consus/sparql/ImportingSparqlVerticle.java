package io.piveau.consus.sparql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.consus.utils.SparqlConnector;
import io.piveau.pipe.PipeContext;
import io.vertx.circuitbreaker.CircuitBreaker;
import io.vertx.circuitbreaker.CircuitBreakerOptions;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

public class ImportingSparqlVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.sparql.queue";

    private WebClient client;

    private CircuitBreaker breaker;

    private int defaultDelay;

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        client = WebClient.create(vertx);

        breaker = CircuitBreaker.create("sparql-breaker", vertx, new CircuitBreakerOptions()
                .setMaxRetries(2)
                .setTimeout(30000))
                .retryPolicy(count -> count * 15000L);

        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray().add("PIVEAU_IMPORTING_SEND_LIST_DELAY")));
        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions().addStore(envStoreOptions));
        retriever.getConfig(ar -> {
            if (ar.succeeded()) {
                defaultDelay = ar.result().getInteger("PIVEAU_IMPORTING_SEND_LIST_DELAY", 8000);
                startPromise.complete();
            } else {
                startPromise.fail(ar.cause());
            }
        });
        retriever.listen(change -> defaultDelay = change.getNewConfiguration().getInteger("PIVEAU_IMPORTING_SEND_LIST_DELAY", 8000));
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonObject config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        String catalogueUriRef = config.getString("catalogueUriRef", "");

        SparqlConnector connector = SparqlConnector.create(client, breaker, pipeContext);

        Promise<Long> countPromise = Promise.promise();
        connector.getTotalCount(catalogueUriRef, countPromise);
        countPromise.future().compose(totalCount -> {
            Promise<Void> finishPromise = Promise.promise();

            connector.init(totalCount);
            connector.fetchPage(catalogueUriRef, finishPromise);

            return finishPromise.future();
        }).setHandler(ar -> {
            if (ar.succeeded()) {
                pipeContext.log().info("Import metadata finished");
                int delay = pipeContext.getConfig().getInteger("sendListDelay", defaultDelay);
                vertx.setTimer(delay, t -> {
                    ObjectNode info = new ObjectMapper().createObjectNode()
                            .put("content", "identifierList")
                            .put("catalogue", config.getString("catalogue"));
                    pipeContext.setResult(new JsonArray(connector.getIdentifiers()).encodePrettily(), "application/json", info).forward();
                });
            } else {
                pipeContext.log().error("Import metadata finished", ar.cause());
            }
        });

    }

}
